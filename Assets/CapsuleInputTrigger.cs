﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleInputTrigger : MonoBehaviour {
	public GameObject tube;

	void OnTriggerEnter(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			capsuleController.inputTarget = tube;
		}
	}

	void OnTriggerStay(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			capsuleController.inputTarget = tube;
		}
	}


	void OnTriggerExit(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			capsuleController.inputTarget = null;
		}
	}
}
