﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeTopCollide : MonoBehaviour {
	public int tubeId;

	void OnTriggerEnter(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			capsuleController.CompleteSend(tubeId);
		}
	}
}
