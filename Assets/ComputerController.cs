﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerController : MonoBehaviour {
	public GameObject Label;
	public GameObject Game;
	public Material[] labelMaterials;
	public GameObject BeepSound;
	public GameObject AlertSound;
	public GameObject AlertLight;
	private int displayTime;
	private int alertTime;

	void Start () {
		Label.SetActive(false);
	}
	
	void Update () {
		if (displayTime > 0) {
			displayTime--;
			if (displayTime == 0) {
				Label.SetActive(false);
			}
		}
		if (alertTime > 0) {
			alertTime--;
			if (alertTime == 0) {
				AlertLight.GetComponent<LightController>().SwitchOff();
			}
		}
	}

	public void ButtonPress(int buttonNumber) {
		Animator animator = GetComponent<Animator>();
		animator.SetTrigger(string.Format("Button{0}Click", buttonNumber));
		BeepSound.GetComponent<AudioSource>().Play();
		GameController gameController = Game.GetComponent<GameController>();

		int dept = buttonNumber * -1;
		int destination = (gameController.ReassignDept == dept)
			? 0
			: gameController.GetDept(dept);
		Debug.Assert(destination >= 0 && destination <= 9, "destination should be 0..9");

		Label.SetActive(true);
		Renderer labelRenderer = Label.GetComponent<Renderer>();
		labelRenderer.material = labelMaterials[destination];
		displayTime = 180;
	}

	public void ShowAlert() {
		AlertSound.GetComponent<AudioSource>().Play();
		AlertLight.GetComponent<LightController>().SwitchOn();
		alertTime = 120;
	}
}
