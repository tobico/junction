﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum State { Startup, Objective, Error, LevelUp, GameOver }
public class GameController : MonoBehaviour {
	public GameObject[] tubes;
	public GameObject[] Lights;
	public GameObject ErrorLight;
	public GameObject ErrorSound;
	public GameObject[] DiscoLights;
	public GameObject LevelUpSound;
	public GameObject Darkness;
	public GameObject Computer;
	public GameObject Player;
	private float spawnInterval;
	private int time;
	private State state;
	private int score;
	private int level;
	private int nextLevelAt;
	private int[] departments;
	private float health;
	private int reassignTime;
	public int ReassignDept = 0;
	private bool levelFirstObjective;
	private int[] levelGates = {0, 3, 6, 12, 18, 30, 42, 60, 80, 100, 120, 140, 160, 180, 200, 500, 1000, 1000000};

	void Start () {
		level = 1;
		levelFirstObjective = true;
		spawnInterval = 900;
		nextLevelAt = levelGates[level];
		score = 0;
		health = 1.0f;
		state = State.Startup;
		time = 0;
		departments = new int[4];
		AssignInitialDepartments();
		reassignTime = GetReassignInterval();

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
	
	public void CapsuleSent(int destination, int sentTo, bool encrypted, bool decrypted) {
		if (sentTo == ResolveDest(destination) && (!encrypted || decrypted)) {
			score += 1; 
			health = Mathf.Lerp(health, 1.0f, 0.2f);
			if (score >= nextLevelAt) {
				LevelUp();
			}
		} else {
			health -= 0.33f;
			ShowError();
		}
		UpdateLightsBrightness();
		DarknessController darknessController = Darkness.GetComponent<DarknessController>();
		darknessController.BaseDarkness = health > 0.4f ? 0.0f : 0.4f - health;
		if (state != State.Error) {
			darknessController.Off();
		}
		
		if (health < 0) {
			GameOver();
		}
	}

	void LevelUp() {
		level += 1;
		nextLevelAt = levelGates[level];		
		if (level % 2 == 0) {
			spawnInterval *= 0.75f;
		}
		levelFirstObjective = true;
		state = State.LevelUp;
		time = 0;
		LevelUpSound.GetComponent<AudioSource>().Play();
		SwitchDiscoLightsOn();
		Debug.Log(string.Format("Level Up: {0}, next at {1}", level, nextLevelAt));
	}

	void Update () {
	    if (Input.GetKey(KeyCode.Escape)) { Application.Quit(); }

		time++;
		switch(state) {
			case State.Startup:
				if (time == 100) {
					UpdateLightsBrightness();
					SwitchLightsOn();
				}
				if (time == 120) {
					AddObjective();
				}
				break;
			case State.Objective:
				if (time >= spawnInterval) {
					AddObjective();
				}
				break;
			case State.Error:
				if (time == 90) {
					SwitchErrorLightOn();
				}
				if (time % 120 == 90) {
					PlayErrorSound();
				}
				if (time == 420) {
					SwitchErrorLightOff();
					SwitchLightsOn();
					AddObjective();
				}
				break;
			case State.LevelUp:
				if (time == 240) {
					SwitchDiscoLightsOff();
					AddObjective();
				}
				break;
			case State.GameOver:
				if (time == 60) {
					SceneManager.LoadScene("Game Over");
				}
				break;
		}

		TickReassign();
	}

	void AddObjective() {
		state = State.Objective;
		time = 0;

		if (level == 2 && levelFirstObjective) {
			AddRandomCapsule(false);
			AddRandomCapsule(false);
			levelFirstObjective = false;
			return;			
		}

		if (level == 3 && levelFirstObjective) {
			AddRandomDeptCapsule(false);
			levelFirstObjective = false;
			return;
		}

		if (level == 4 && levelFirstObjective) {
			AddRandomDeptCapsule(false);
			AddRandomDeptCapsule(false);
			levelFirstObjective = false;
			return;
		}

		if (level == 5 && levelFirstObjective) {
			AddRandomCapsule(true);
			levelFirstObjective = false;
			return;
		}

		if (level == 6 && levelFirstObjective) {
			AddRandomCapsule(true);
			AddRandomCapsule(true);
			AddRandomCapsule(true);
			levelFirstObjective = false;
			return;
		}

		if (level == 7 && levelFirstObjective) {
			AddRandomDeptCapsule(true);
			levelFirstObjective = false;
			return;
		}

		if (level == 8 && levelFirstObjective) {
			AddRandomDeptCapsule(true);
			AddRandomDeptCapsule(true);
			AddRandomDeptCapsule(true);
			AddRandomDeptCapsule(true);
			levelFirstObjective = false;
			return;
		}
		

		if (level < 3) {
			AddRandomCapsule(false);
			levelFirstObjective = false;
			return;
		}

		int maxType = 2;
		if (level >= 5) {
			maxType = 3;
		}
		if (level >= 7) {
			maxType = 4;
		}

		switch (Random.Range(0, maxType)) {
			case 0:
				AddRandomCapsule(false);
				break;
			case 1:
				AddRandomDeptCapsule(false);
				break;
			case 2:
				AddRandomCapsule(true);
				break;
			case 3:
				AddRandomDeptCapsule(true);
				break;
		}
		levelFirstObjective = false;
	}

	void ShowError() {
		state = State.Error;
		SwitchLightsOff();
		time = 0;
	}

	void SwitchLightsOn() {
		foreach (GameObject lightObject in Lights) {
			LightController lightController = lightObject.GetComponent<LightController>();
			lightController.SwitchOn();
		}
		DarknessOff();
	}

	void SwitchLightsOff() {
		foreach (GameObject lightObject in Lights) {
			LightController lightController = lightObject.GetComponent<LightController>();
			lightController.SwitchOff();
		}
		DarknessOn();
	}

	void SwitchDiscoLightsOn() {
		foreach (GameObject lightObject in DiscoLights) {
			DiscoLightController lightController = lightObject.GetComponent<DiscoLightController>();
			lightController.SwitchOn();
		}
	}

	void SwitchDiscoLightsOff() {
		foreach (GameObject lightObject in DiscoLights) {
			DiscoLightController lightController = lightObject.GetComponent<DiscoLightController>();
			lightController.SwitchOff();
		}
	}

	void UpdateLightsBrightness() {
		foreach (GameObject lightObject in Lights) {
			LightController lightController = lightObject.GetComponent<LightController>();
			lightController.baseBrightness = health;
		}
	}

	void SwitchErrorLightOn() {
		LightController lightController = ErrorLight.GetComponent<LightController>();
		lightController.SwitchOn();
		DarknessOff();
	}

	void DarknessOn() {
		Darkness.GetComponent<DarknessController>().On();
		Player.GetComponent<PlayerController>().ControlsEnabled = false;
	}

	void DarknessOff() {
		Darkness.GetComponent<DarknessController>().Off();
		Player.GetComponent<PlayerController>().ControlsEnabled = true;
	}

	void SwitchErrorLightOff() {
		LightController lightController = ErrorLight.GetComponent<LightController>();
		lightController.SwitchOff();
	}

	void PlayErrorSound() {
		AudioSource audioSource = ErrorSound.GetComponent<AudioSource>();
		audioSource.Play();
	}

	void AddRandomCapsule(bool encrypted) {
		int source = Random.Range(1, 10);
		int destination = source;
		while (destination == source) {
			destination = Random.Range(1, 10);
		}
		SpawnCapsule(source, destination, encrypted);
	}

	void SpawnCapsule(int tube, int value, bool encrypted) {
		TubeController tubeController = GetTubeController(tube - 1);
		GameObject capsule = tubeController.SpawnCapsule();
		CapsuleController capsuleController = capsule.GetComponent<CapsuleController>();
		capsuleController.GameController = this;
		capsuleController.SetEncrypted(encrypted);
		capsuleController.SetDestination(value);
	}

	TubeController GetTubeController(int index) {
		GameObject tube = tubes[index];
		return tube.GetComponent<TubeController>();
	}

	void GameOver() {
		state = State.GameOver;
		time = 0;
		Darkness.GetComponent<DarknessController>().On();
	}

	/**
	 * Departments
	 */

	void AddRandomDeptCapsule(bool encrypted) {
		int dept = Random.Range(-4, 0);
		int currentDest = GetDept(dept);
		int source = currentDest;
		while (source == currentDest) {
			source = Random.Range(1, 10);
		}
		SpawnCapsule(source, dept, encrypted);
	}

	public int GetDept(int dept) {
		return departments[dept * -1 - 1];
	}

	int ResolveDest(int destination) {
		if (destination < 0) {
			return GetDept(destination);
		} else {
			return destination;
		}
	}

	void AssignInitialDepartments() {
		for (int i = 0; i < 4; i++) {
			departments[i] = Random.Range(1, 10);
		}
	}

	int GetReassignInterval() {
		return 2000 - 100 * level;
	}

	void TickReassign() {
		if (reassignTime > 0) {
			reassignTime--;
		} else {
			reassignTime = GetReassignInterval();
			DoReassign();
		}
	}

	void DoReassign() {
		Computer.GetComponent<ComputerController>().ShowAlert();
		if (ReassignDept < 0) {
			departments[ReassignDept * -1 - 1] = Random.Range(1, 10);
		}
		int oldReassignDept = ReassignDept;
		while (oldReassignDept == ReassignDept) {
			ReassignDept = Random.Range(-4, 0);
		}
	}
}
