﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerButtonController : MonoBehaviour {
	public GameObject Computer;
	public int ButtonNumber;

	public void TriggerComputer() {
		ComputerController computerController = Computer.GetComponent<ComputerController>();
		computerController.ButtonPress(ButtonNumber);
	}
}
