﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HopperState {
	Idle,
	Checking,
	Valid,
	Invalid
}

public class HopperController : MonoBehaviour {
	public GameObject InvalidSound;
	public GameObject ValidSound;
	public bool open = true;
	private HopperState state;
	private GameObject insertedCapsule;
	private GameObject collidingCapsule;
	private int time;

	public void CapsuleEnter(GameObject capsule) {
		collidingCapsule = capsule;
	}

	public void CapsuleExit(GameObject capsule) {
		if (capsule != null && capsule == insertedCapsule) {
			insertedCapsule = null;
			state = HopperState.Idle;
		} 
		collidingCapsule = null;
	}

	public HopperState GetState() {
		return state;
	}

	public GameObject GetInsertedCapsule() {
		return insertedCapsule;
	}

	void Start () {
		state = HopperState.Idle;
	}
	
	void Update () {
		switch (state) {
			case HopperState.Idle:
				if (collidingCapsule != null) {
					if (!collidingCapsule.GetComponent<CapsuleController>().carried) {
						state = HopperState.Checking;
						time = 60;
					}
				}
				break;
			case HopperState.Checking:
				if (time > 0) {
					time--;
				} else {
					if (collidingCapsule != null) {
						CapsuleController capsuleController = collidingCapsule.GetComponent<CapsuleController>();
						if (capsuleController.encrypted == false || capsuleController.decrypted == true) {
							state = HopperState.Invalid;
							if (open) {
								collidingCapsule.GetComponent<Rigidbody>().velocity = transform.forward * 10.0f;
								InvalidSound.GetComponent<AudioSource>().Play();
							}
							time = 60;
						} else {
							insertedCapsule = collidingCapsule;
							state = HopperState.Valid;
							if (open) {
								ValidSound.GetComponent<AudioSource>().Play();
							}							
						}
					}
				}
				break;
			case HopperState.Invalid:
				if (time > 0) {
					time--;
				} else {
					state = HopperState.Idle;
				}
				break;
		}
	}
}
