﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopperTrigger : MonoBehaviour {
	public GameObject Hopper;

	void OnTriggerEnter(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			Hopper.GetComponent<HopperController>().CapsuleEnter(other.gameObject);
		}
	}

	void OnTriggerExit(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			Hopper.GetComponent<HopperController>().CapsuleExit(other.gameObject);
		}
	}
}
