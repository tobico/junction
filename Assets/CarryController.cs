﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryController : MonoBehaviour {
    public float HoldDistance = 4.0f;
    public float MinCarryDistance = 0.2f;
    public float MaxCarryDistance = 1.0f;
    public Camera mainCamera;
    private GameObject carriedObject;
    private float hover = 0;
    private int wallLayerMask;
 
	void Start () {
        wallLayerMask = 1 << LayerMask.NameToLayer("Walls");
	}
	
	void Update () {
        if (carriedObject != null) {
            carryObject(carriedObject);
            if (Input.GetButtonDown("Fire1")) {
                dropObject(carriedObject);
            }
        } else {
            if (Input.GetButtonDown("Fire1")) {
                pickup();
            }
        }
    }

    void carryObject(GameObject gameObject) {
        Rigidbody body = gameObject.GetComponent<Rigidbody>();
        body.isKinematic = true;
        body.transform.position = mainCamera.transform.position + mainCamera.transform.forward * 
            Mathf.Min(getWallDistance(), HoldDistance);
        if (getCarriedCapsuleController().inputTarget != null) {
            body.transform.position += mainCamera.transform.up * 0.08f * Mathf.Sin(hover);
            hover += 0.2f;
            if (hover > Mathf.PI * 2.0f) {
                hover -= Mathf.PI * 2.0f;
            }
        }
        body.transform.rotation = transform.rotation;
        body.transform.Rotate(new Vector3(90f, 90f, 180f));
    }

    float getWallDistance() {
        Ray ray = makeCrosshairRay();
        // ray.origin = ray.origin + mainCamera.transform.forward * MinCarryDistance;
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100.0f, wallLayerMask)) {
            return hit.distance;
        } else {
            return Mathf.Infinity;
        } 
    }

    void dropObject(GameObject gameObject) {
        CapsuleController capsuleController = getCarriedCapsuleController(); 
        Rigidbody body = gameObject.GetComponent<Rigidbody>();
        body.isKinematic = false;
        capsuleController.carried = false;
        carriedObject = null;

        GameObject inputTarget = capsuleController.inputTarget;
        if (inputTarget != null) {
            TubeController tubeController = inputTarget.GetComponent<TubeController>();
            if (tubeController != null) {
                tubeController.SendCapsule(gameObject);
            }
        }
    }

    CapsuleController getCarriedCapsuleController() {
        return carriedObject.GetComponent<CapsuleController>();
    }

    void pickup() {
        Ray ray = makeCrosshairRay();
        ray.origin = ray.origin + mainCamera.transform.forward * MinCarryDistance;
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            CapsuleController capsuleController = hit.collider.GetComponent<CapsuleController>();
            if (capsuleController != null) {
                carriedObject = hit.collider.gameObject;
                capsuleController.carried = true;
            }
            ComputerButtonController buttonController = hit.collider.GetComponent<ComputerButtonController>();
            if (buttonController != null) {
                buttonController.TriggerComputer();
            }
        }
    }

    Ray makeCrosshairRay() {
        int x = Screen.width / 2;
        int y = Screen.height / 2;
        return mainCamera.ScreenPointToRay(new Vector3(x, y));
    }
}
