﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum MachineState {
	Ready,
	Active,
	Done,
	Return
}

public class MachineController : MonoBehaviour {
	public GameObject Hoppers;
	public GameObject[] HopperObjects;
	public GameObject[] ValidLights;
	public GameObject MachineSound;
	private float hopperPosition;
	private MachineState state;
	private int time;
	private int lightsOn = 0;

	void Start() {
		state = MachineState.Ready;
		hopperPosition = -1.5f;
	}
	
	void Update() {
		Vector3 position;

		switch (state) {
			case MachineState.Ready:
				int hoppersFull = CountHoppersFull();
				if (lightsOn != hoppersFull) {
					UpdateLights(hoppersFull);
				}
				if (hoppersFull == 3) {
					state = MachineState.Active;
					MachineSound.GetComponent<AudioSource>().Play();
					SetHoppersOpen(false);
					time = 0;
				}
				break;
			case MachineState.Active:
				hopperPosition += 0.01f;
				position = Hoppers.transform.position;
				position.z = hopperPosition;
				Hoppers.transform.position = position;
				if (hopperPosition >= 1.5f) {
					state = MachineState.Done;
					MachineSound.GetComponent<AudioSource>().Stop();
					UpdateLights(0);
				}
				time++;
				if (time == 60) {
					DecryptCapsules();
				}
				break;
			case MachineState.Done:
				if (HoppersEmpty()) {
					state = MachineState.Return;
				}
				break;
			case MachineState.Return:
				hopperPosition -= 0.02f;
				position = Hoppers.transform.position;
				position.z = hopperPosition;
				Hoppers.transform.position = position;
				if (hopperPosition <= -1.5f) {
					state = MachineState.Ready;
					SetHoppersOpen(true);
				}
				break;
		}
	}

	int CountHoppersFull() {
		int count = 0;
		foreach (GameObject hopper in HopperObjects) {
			if (hopper.GetComponent<HopperController>().GetState() == HopperState.Valid) {
				count++;
			}
		}
		return count;
	}

	bool HoppersEmpty() {
		bool result = true;
		foreach (GameObject hopper in HopperObjects) {
			if (hopper.GetComponent<HopperController>().GetState() != HopperState.Idle) {
				result = false;
			}
		}
		return result;
	}

	void SetHoppersOpen(bool open) {
		foreach (GameObject hopper in HopperObjects) {
			hopper.GetComponent<HopperController>().open = open;
		}
	}

	void UpdateLights(int value) {
		lightsOn = value;
		for (var i = 1; i <= 3; i++) {
			LightController controller = ValidLights[i - 1].GetComponent<LightController>();
			if (lightsOn >= i) {
				controller.SwitchOn();
			} else {
				controller.SwitchOff();
			}
		}
	}

	void DecryptCapsules() {
		foreach (GameObject hopper in HopperObjects) {
			GameObject capsule = hopper.GetComponent<HopperController>().GetInsertedCapsule();
			if (capsule != null) {
				CapsuleController capsuleController = capsule.GetComponent<CapsuleController>();
				Debug.Assert(capsuleController != null, "capsule must have controller");
				if (capsuleController != null && capsuleController.encrypted) {
					capsuleController.SetDecrypted(true);
				}
			}
		}	
	}
}
