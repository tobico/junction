﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour {
	private int time;

	void Start () {
		time = 0;
	}
	
	void Update () {
		time++;
		if (time % 60 == 0) {
			Renderer renderer = GetComponent<Renderer>();
			renderer.enabled = !renderer.enabled;
		}
	}
}
