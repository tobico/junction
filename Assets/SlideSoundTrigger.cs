﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideSoundTrigger : MonoBehaviour {
	void OnTriggerEnter(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			AudioSource audioSource = capsuleController.SlideSound.GetComponent<AudioSource>();
			audioSource.Play();
		}
	}

	void OnTriggerExit(Collider other) {
		CapsuleController capsuleController = other.GetComponent<CapsuleController>();
		if (capsuleController != null) {
			AudioSource audioSource = capsuleController.SlideSound.GetComponent<AudioSource>();
			audioSource.Stop();
		}
	}
}
