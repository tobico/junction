﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoLightController : MonoBehaviour {
	public GameObject LightObject;
	private float time;
	private bool on = false;
	public Material offMaterial;
	public Material onMaterial;

	void Start() {
		time = 0;
		SetRandomColor();
	}
	
	void Update() {
		if (on) {
			time += 1;
			if (time % 60 == 0) {
				SetRandomColor();
			}
		}
	}

	public void SwitchOn() {
		Renderer renderer = GetComponent<Renderer>();
		renderer.material = onMaterial;
		Light light = LightObject.GetComponent<Light>();
		light.intensity = 20.0f;
		on = true;
	}

	public void SwitchOff() {
		Renderer renderer = GetComponent<Renderer>();
		renderer.material = offMaterial;
		Light light = LightObject.GetComponent<Light>();
		light.intensity = 0.0f;
		on = false;
	}

	void SetRandomColor() {
		Light light = LightObject.GetComponent<Light>();
		Color color = Random.ColorHSV(0.0f, 1.0f);
		light.color = color;
	}
}
