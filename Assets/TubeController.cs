﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeController : MonoBehaviour {
    public GameObject CapsulePrefab;
    public GameObject CapsuleSpawnPoint;
    public GameObject CapsuleSendPoint;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject SpawnCapsule() {
        GameObject capsule = Instantiate(CapsulePrefab, CapsuleSpawnPoint.transform.position, CapsuleSpawnPoint.transform.rotation);
        return capsule;
    }

    public void SendCapsule(GameObject capsule) {
        CapsuleController controller = capsule.GetComponent<CapsuleController>();
        controller.StartSend();
        capsule.transform.position = CapsuleSendPoint.transform.position;
        capsule.transform.rotation = CapsuleSendPoint.transform.rotation;
    }
}
