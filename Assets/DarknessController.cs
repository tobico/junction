﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessController : MonoBehaviour {
	public float TargetOpacity;
	public float BaseDarkness = 0.0f;
	float opacity;

	void Start() {
		opacity = 1;
	}

	public void On() {
		TargetOpacity = 1.0f;
	}
	
	public void Off() {
		TargetOpacity = BaseDarkness;
	}
	
	void Update() {
		opacity = Mathf.Max(Mathf.Lerp(opacity, TargetOpacity, 0.05f), BaseDarkness);
		Renderer renderer = GetComponent<Renderer>();
		renderer.material.SetColor("_TintColor", new Color(0, 0, 0, opacity));
	}
}
