﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleController : MonoBehaviour {
	public Material[] labelMaterials;
	public GameObject label;
	public bool carried;
	public bool encrypted;
	public bool decrypted;
	private bool outgoing;
	public GameObject inputTarget;
	private int destination;
	public GameController GameController;
	public GameObject SlideSound;
	public GameObject ThudSound;

	void Start() {
	}

	void Update() {
		Rigidbody body = GetComponent<Rigidbody>();
		if (body.velocity.z < 0) {
			body.useGravity = true;
		}
		AudioSource slideAudioSource = SlideSound.GetComponent<AudioSource>();
		slideAudioSource.volume = Mathf.Clamp(body.velocity.sqrMagnitude, 0.0f, 1.0f);
	}

	void OnCollisionEnter(Collision collision) {
		float magnitude = collision.relativeVelocity.sqrMagnitude;
		if (magnitude > 1.0f) {
			AudioSource audioSource = ThudSound.GetComponent<AudioSource>();
			audioSource.volume = Mathf.Clamp01((magnitude - 1.0f) / 10.0f);
			audioSource.Play();
		}
	}

	public int GetDestination() {
		return destination;
	}

	public void SetDestination(int number) {
		destination = number;
		UpdateLabel();
	}

	public void SetEncrypted(bool value) {
		encrypted = value;
		UpdateLabel();
	}

	public void SetDecrypted(bool value) {
		decrypted = value;
		UpdateLabel();
	}

	public void StartSend() {
		outgoing = true;
		Rigidbody body = GetComponent<Rigidbody>();
        body.useGravity = false;
        body.velocity = new Vector3(0f, 12.0f, 0f);
	}

	public void CompleteSend(int number) {
		if (outgoing) {
			DestroyObject(gameObject);
			GameController.CapsuleSent(destination, number, encrypted, decrypted);
		} 
	}

	private void UpdateLabel() {
		Renderer renderer = label.GetComponent<Renderer>();
		if (destination < 0) {
			if (decrypted) {
				renderer.material = labelMaterials[10];
			} else if (encrypted) {
				renderer.material = labelMaterials[23 + destination * -1];
			} else {
				renderer.material = labelMaterials[19 + destination * -1];
			}
		} else {
			if (decrypted) {
				renderer.material = labelMaterials[10];
			} else if (encrypted) {
				renderer.material = labelMaterials[destination + 10];
			} else {
				renderer.material = labelMaterials[destination];
			}
		}
	}
}
