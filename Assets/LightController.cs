﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {
	public float phase = 0;
	public float baseBrightness = 1.8f;
	public float glowAmplitude = 0.5f;
	public float lightLevel = 0;
	public GameObject[] lights;
	private float glow;
	public bool on = false;
	public Material offMaterial;
	public Material onMaterial;
	public GameObject OnSound; 
	public GameObject OffSound; 

	void Start() {
		glow = phase;
		SetLightIntensity(0);
		lightLevel = 0;
	}

	public void SwitchOn() {
		Renderer renderer = GetComponent<Renderer>();
		renderer.material = onMaterial;
		on = true;
		playSound(OnSound);
	}

	public void SwitchOff() {
		Renderer renderer = GetComponent<Renderer>();
		renderer.material = offMaterial;
		on = false;
		SetLightIntensity(0);
		lightLevel = 0;
		playSound(OffSound);
	}

	void playSound(GameObject sound) {
		if (sound == null) return;
		
		AudioSource audioSource = sound.GetComponent<AudioSource>();
		if (audioSource != null) {
			audioSource.Play();
		}
	}
	
	void Update() {
		if (on) {
			glow += 0.05f;
			float desiredIntensity = baseBrightness + Mathf.Sin(glow) * glowAmplitude; 
			lightLevel = Mathf.Lerp(lightLevel, desiredIntensity, 0.25f);
			SetLightIntensity(lightLevel);
		}
	}

	void SetLightIntensity(float intensity) {
		foreach (GameObject lightObject in lights) {
			Light light = lightObject.GetComponent<Light>();
			light.intensity = intensity;
		}
	}
}
