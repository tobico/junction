﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour {
    public float phase = 0;
    public float interval = 30;
    public float amplitude = 10;
    private float x;
    Quaternion initalRotation;

	// Use this for initialization
	void Start () {
        x = phase * getStep();
        initalRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        x += getStep();
        float z = Mathf.Sin(x) * amplitude;
        transform.rotation = initalRotation;
        transform.Rotate(0, 0, z);
	}

    float getStep() {
        return Mathf.PI * 2 / interval;
    }
}
